import java.lang.reflect.Array;
import java.util.*;

public class Exercise3 {
    public static void main(String[] args){

        int length=9;
        String word = null;
        ArrayList<String> RandomWords = new ArrayList<>();
        Random random = new Random();





        for (int i=0;i < 5;i++){

             word = random.ints(48,122)//48 (unicode for 0) ,122 (unicode for z)
                    .filter(j-> (j<57 || j>65) && (j <90 || j>97))//digits 0-9, letters A-Z
                    .mapToObj(j -> (char) j)//convert numbers to chars
                    .limit(length)
                    .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)//collect to StringBuilder
                    .toString();

            RandomWords.add(word);

        }

        RandomWords.stream().forEach(System.out::println);

        System.out.println("1.Filter out 2 words using Lambda Expressions:");
        RandomWords.stream().sorted((a,b)->a.compareTo(b)).forEach(System.out::println);

        System.out.println("1.Filter out 2 words  using Method References:");
        RandomWords.stream().sorted(String::compareTo).forEach(System.out::println);

    }
}
