import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Exercise1 {

    public static void main(String[] args) {

        List<String> CityList = Arrays.asList("Athens", "Thessaloniki", "Patras", "Larisa", "Kozani", "Karditsa", "Trikala", "Kalamata",
                "Tripoli", "Rethymno");

        System.out.println("1.Alphabetical sorted:");
        CityList.stream().sorted().forEach(System.out::println);

        System.out.println("2.list in length descending order: ");
        Comparator<String> DescByLength = (a,b) -> b.length()- a.length();// Comparator for  Descending order.
        CityList.stream().sorted(DescByLength).forEach(System.out::println);

        System.out.println("3.first letter of each word without duplicates:");
        CityList.stream().map(n->n.charAt(0)).distinct().forEach(System.out::println);

        System.out.println("4.list in all CAPITALS:");
        CityList.stream().map(n->n.toUpperCase()).forEach(System.out::println);

        System.out.println("5.all the words starting with K:");
        CityList.stream().filter(n->n.startsWith("K")).forEach(System.out::println);

        System.out.println("6.all the words excluding words starting with K or T:");
        CityList.stream().filter(n->!n.startsWith("K") && !n.startsWith("T")).forEach(System.out::println);

        System.out.println("7.the first word the most letters:");
        CityList.stream().sorted(DescByLength).limit(1).forEach(System.out::println);

        System.out.println("8.the first word the least letters:");
        Comparator<String> AscByLength = (a,b) -> a.length()-b.length();//Comparator for Ascending order.
        CityList.stream().sorted(AscByLength).limit(1).forEach(System.out::println);

    }
}
