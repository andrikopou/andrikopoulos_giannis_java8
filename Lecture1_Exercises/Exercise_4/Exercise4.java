import java.lang.reflect.Array;
import java.math.BigInteger;
import java.util.ArrayList;

public class Exercise4 {

    public static void main(String[] args){

        ArrayList<Integer> factorials = new ArrayList<>();
        factorials.add(10);
        factorials.add(15);
        factorials.add(32);
        factorials.add(50);
        //factorial(BigInteger.valueOf(10));


       factorials.stream().map(s->factorial(BigInteger.valueOf(s))).forEach(System.out::println);
    }

    /*public static int factorial(int n){

        for (int i = n; i>1; i--){
            n=n*(i-1);
            System.out.println(n);
        }
        return n;
    }*/

    public static BigInteger factorial(BigInteger n){
        BigInteger j= BigInteger.valueOf(1);

        for (BigInteger i = n; i.compareTo(BigInteger.ONE)>0; i=i.subtract(BigInteger.ONE)){
            n=n.multiply(i.subtract(BigInteger.ONE));
            //System.out.println(n);
        }
        return n;
    }
}
