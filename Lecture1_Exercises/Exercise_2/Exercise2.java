import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Exercise2 {

    public static void main(String[] args){
        ArrayList<Person> Persons = new ArrayList<Person>();
        Person person1 = new Person("Mike", 29);
        Person person2 = new Person("John", 30);
        Person person3 = new Person("John", 32);
        Person person4 = new Person("Jack", 20);
        Person person5 = new Person("Sam", 20);
        Person person6 = new Person("Bob", 31);
        Person person7 = new Person("Bob", 20);

        Persons.add(person1);
        Persons.add(person2);
        Persons.add(person3);
        Persons.add(person4);
        Persons.add(person5);
        Persons.add(person6);
        Persons.add(person7);

        System.out.println("1.ages of all the Persons:");
        Persons.stream().forEach(n->System.out.println(n.age));

        System.out.println("2.name of the Person with the greatest age:");
        Comparator<Person> DescByAge = (a,b)-> b.age - a.age;
        Persons.stream().sorted(DescByAge).limit(1).forEach(n->System.out.println(n.name));

        System.out.println("3. Map using as a Key the name of a Person and as a Value a List of Persons with said name:");
        Persons.stream().collect(Collectors.groupingBy(Person::getName,Collectors.toList())).forEach((n,s)->System.out.println(n+","+s));

        System.out.println("4. a comma separated String that will contain the ages of all the Persons in ascending order sans duplicates:");
        Comparator<Person> AscByAge = (a,b) -> a.age - b.age; //Comparator for Ascending order.
        String string = Persons.stream().sorted(AscByAge).map(Person::getAgeToString).distinct().collect(Collectors.joining(","));
        System.out.println(string);

        System.out.println("5.a comma separated String that will contain the ages of all the Persons multiplied by 10 in descending order:");
        String s1 = Persons.stream().sorted(DescByAge).map(Person::getAge).map(n->n*10).map(n->n.toString()).collect(Collectors.joining(","));
        System.out.println(s1);

        System.out.println("6. a Map using as a Key the name of a Person and as a Value a List of ages of Persons with said name:");
        Persons.stream().collect(Collectors.groupingBy(Person::getName,Collectors.mapping(Person::getAge,Collectors.toSet()))).forEach((n,s)->System.out.println("'"+n+"'->"+s ));

        System.out.println("7.a Map using as a Key the age of a Person and as a Value the count of Persons that have said age:");
        Persons.stream().collect(Collectors.groupingBy(Person::getAge,Collectors.counting())).forEach((n,s)->System.out.println(n+","+s));







    }
}